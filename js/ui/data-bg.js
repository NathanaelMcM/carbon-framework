/**
 * Data BG
 *
 * Quickly add background images to your sections using this awesome
 * script.
*/

// Define vars
var bgClass     = '.has-background';
var defaultPos  = 'center';
var attachment  = 'scroll';
var height      = 'auto';

// Run the loop on the bgClass
  $(bgClass).each(function(){
    var bgImg = $(this).data('bg-img');
    var bgAtt = $(this).data('bg-attachment') === undefined ? attachment : $(this).data('bg-attachment');
    var bgHeight = $(this).data('bg-height') === undefined ? height : $(this).data('bg-height');
    var bgPos = $(this).data('bg-pos') === undefined ? defaultPos : $(this).data('bg-pos');
    $(this).css({
        "background-image": "url(" + bgImg + ")",
        "background-position": bgPos,
        "background-attachment" : bgAtt,
        "min-height": bgHeight
    });
  });
